Various examples from the [Rust book](https://doc.rust-lang.org/book).

Each branch is one example:
* [Guessing Game](https://gitlab.com/riribreizh/rust_tutorials/tree/guessing_game), chapter 2
* [Rectangles](https://gitlab.com/riribreizh/rust_tutorials/tree/rectangles), chapter 5
